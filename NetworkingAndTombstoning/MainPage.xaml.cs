﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;
using Microsoft.Phone.Controls;
using Microsoft.Phone.Shell;
using System.Collections.ObjectModel;
using System.Xml.Linq;
using System.IO.IsolatedStorage;
using System.Globalization;

namespace NetworkingAndTombstoning
{
    public partial class MainPage : PhoneApplicationPage
    {
        private string weatherURL = "http://api.worldweatheronline.com/free/v1/weather.ashx?q=";

        private String APIKEY = "gxsv6pgx4nmx96nu88skcetf";

        Forecast forecast;
        List<Forecast> forecasts;

        private String query;

        // Constructor
        public MainPage()
        {
            InitializeComponent();

            // is there network connection available
            if (!System.Net.NetworkInformation.NetworkInterface.GetIsNetworkAvailable())
            {
                MessageBox.Show("There is no network connection available!");
                return;
            }
        }

        // event handler for the search button click, it takes the city name and retrieves the weather for next 5 days
        private void SearchCityButton_Click(object sender, RoutedEventArgs e)
        {
            query = CityName.Text;
            CityName.Text = "Testing, please wait...";

            WebClient downloader = new WebClient();
            Uri uri = new Uri(weatherURL + query + "&format=xml&num_of_days=5&key=" + APIKEY, UriKind.Absolute);
            downloader.DownloadStringCompleted += new DownloadStringCompletedEventHandler(ForecastDownloaded);
            downloader.DownloadStringAsync(uri);
        }

        // check if the forecast can be retrived from the api, if yes then takes the necessary part and poulates the forecast object
        private void ForecastDownloaded(object sender, DownloadStringCompletedEventArgs e)
        {
            (Application.Current as App).flag = 0;

            if (e.Result == null || e.Error != null)
            {
                MessageBox.Show("Cannot load Weather Forecast!");
            }
            else
            {
                XDocument document = XDocument.Parse(e.Result);
                XElement xmlRoot = document.Root;
                if (xmlRoot.Descendants("error").Count() > 0)
                {
                    MessageBox.Show("There is no weather forecast available for " + query + " or your apikey is wrong!");
                    CityName.Text = query;
                    return;
                }
                var data1 = from query in document.Descendants("current_condition")
                            select new Forecast
                            {
                                observation_time = (string)query.Element("observation_time"),
                                temp_C = (string)query.Element("temp_C"),
                                weatherIconUrl = (string)query.Element("weatherIconUrl"),
                                humidity = (string)query.Element("humidity"),
                                windspeedKmph = (string)query.Element("windspeedKmph")
                            };
                forecast = data1.ToList<Forecast>()[0];

                var data2 = from query in document.Descendants("weather")
                            select new Forecast
                            {
                                date = (string)query.Element("date"),
                                tempMaxC = (string)query.Element("tempMaxC"),
                                tempMinC = (string)query.Element("tempMinC"),
                                weatherIconUrl = (string)query.Element("weatherIconUrl"),
                            };
                forecasts = data2.ToList<Forecast>();

                for (int i = 0; i < forecasts.Count(); i++)
                {
                    forecasts[i].date = DateTime.Parse(forecasts[i].date).ToString("dddd");
                }

                // after populating the forecast object, the focus moves to weatherinfopage to show the UI
                 NavigationService.Navigate(new Uri("/WeatherInfoPage.xaml", UriKind.Relative));

                 // clears the city name text field
                 CityName.Text = "";
            }
        }

        // when navigating away from this page it saves the forecasts and forecast so that it can be used in weatherinfopage.xaml
        protected override void OnNavigatedFrom(System.Windows.Navigation.NavigationEventArgs e)
        {
            PhoneApplicationService.Current.State["forecasts"] = forecasts;
            PhoneApplicationService.Current.State["forecast"] = forecast;
        }
    }
}