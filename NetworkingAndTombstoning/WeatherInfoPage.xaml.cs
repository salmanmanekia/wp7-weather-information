﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;
using Microsoft.Phone.Controls;
using Microsoft.Phone.Shell;
using System.Collections.ObjectModel;
using System.Xml.Linq;
using System.IO.IsolatedStorage;
using System.Globalization;

namespace NetworkingAndTombstoning
{
    public partial class WeatherInfoPage
    {
        Forecast forecast;
        List<Forecast> forecasts;
        const string QueriesSettingsKey = "QueriesKey";

        WeatherPageObject tombedWPO;
        WeatherPageObject wpo;

        public WeatherInfoPage()
        {
            InitializeComponent();
            wpo = new WeatherPageObject();
        }
        
        protected override void OnNavigatedTo(System.Windows.Navigation.NavigationEventArgs e)
        {
            // retrieved forecasts and froecast when navigated from main page
            var forecastsObj = PhoneApplicationService.Current.State["forecasts"];
            var forecastObj = PhoneApplicationService.Current.State["forecast"];
            int flag = (Application.Current as App).flag;

            forecasts = (List <Forecast>)forecastsObj;
            forecast = (Forecast)forecastObj;

            tombedWPO = (Application.Current as App).weatherPO;
            // checks if the user has came to this page from a tombstate
            // if yes, then create the datacontext and itemsource
            // else populate the weather pageobject and then create a data contect and items source
            if (tombedWPO != null && flag == 1)
            {
                WeatherInfoTopBar.DataContext = tombedWPO;
                WeatherInfoListBox.ItemsSource = tombedWPO.forecasts;
            }
            else
                AddPageItem(forecast, forecasts);
        }

      
        // populating and adding the item to the page
        private void AddPageItem(Forecast forecast, List<Forecast> forecasts)
        {
            wpo.temperature = "Temperature: " + forecast.temp_C + " °C";
            wpo.observation_time = "Observ. Time: " + forecast.observation_time;
            wpo.windspeed = "Wind Speed: " + forecast.windspeedKmph + " Kmph";
            wpo.huminity = "Huminity: " + forecast.humidity + " %";
            wpo.weatherIconUrl = forecast.weatherIconUrl;
            wpo.forecasts = forecasts;
            WeatherInfoTopBar.DataContext = wpo;
            WeatherInfoListBox.ItemsSource = wpo.forecasts;

            (Application.Current as App).weatherPO = wpo;

        }
    }
}