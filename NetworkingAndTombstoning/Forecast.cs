﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace NetworkingAndTombstoning
{
    public class Forecast
    {
        public string query { get; set; }
        public string observation_time { get; set; }
        public string date { get; set; }
        public string temp_C { get; set; }
        public string tempMaxC { get; set; }
        public string tempMinC { get; set; }
        public string weatherIconUrl { get; set; }
        public string windspeedKmph { get; set; }
        public string humidity { get; set; }
    }
}
