﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace NetworkingAndTombstoning
{
    public class WeatherPageObject
    {
        public string observation_time { get; set; }
        public string date { get; set; }
        public string temperature { get; set; }
        public string huminity { get; set; }
        public string windspeed { get; set; }
        public string weatherIconUrl { get; set; }
        // five day's forecast
        public List<Forecast> forecasts { get; set; }

        public WeatherPageObject() {
            this.observation_time = "";
            this.date = "";
            this.temperature = "";
            this.huminity = "";
            this.windspeed = "";
            this.weatherIconUrl = "";
            this.forecasts = null;
        }

        // for tombstoning, loading all the values saved by isolated storage in the app
        public WeatherPageObject(string observation_time, string date, string temperature, string huminity, string windspeed, string weatherIconUrl, List<Forecast> forecasts) {
            this.observation_time = observation_time;
            this.date = date;
            this.temperature = temperature;
            this.huminity = huminity;
            this.windspeed = windspeed;
            this.weatherIconUrl = weatherIconUrl;
            this.forecasts = forecasts;
        }

  
    }
}
